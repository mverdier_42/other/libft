/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_search_params.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:01:13 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:01:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_search.h"

t_params	*search_param(const char *format)
{
	int			i;
	t_params	*params;
	t_params	*param;

	i = 0;
	params = NULL;
	while (format[i])
	{
		if (format[i] == '%')
		{
			param = push_back_new(&params);
			i++;
			search_option(format, &i, param);
		}
		else
			i++;
	}
	return (params);
}
