/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_fill_types.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:59:37 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/08 20:14:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_fill.h"

static void	fill_unsigned(char *buf, int *j, t_args *arg, t_params *param)
{
	if (arg_is_zero(arg) == 1 && param->prec == 0 && param->is_prec == true
			&& (param->hash == 0 || (param->hash == 1 && (param->type == 'x'
						|| param->type == 'X' || param->type == 'p'))))
		return ;
	else if (param->type == 'o' || param->type == 'O')
		fill_uintmax_to_a(arg->arg.ui, 8, buf, j);
	else if (param->type == 'x')
		fill_uintmax_to_a(arg->arg.ui, 16, buf, j);
	else if (param->type == 'X')
		fill_uintmax_to_a(arg->arg.ui, 17, buf, j);
	else if (param->type == 'p')
		fill_uintmax_to_a(arg->arg.ui, 16, buf, j);
	else if (param->type == 'b')
		fill_uintmax_to_a(arg->arg.ui, 2, buf, j);
	else
		fill_uintmax_to_a(arg->arg.ui, 10, buf, j);
}

static void	fill_signed(char *buf, int *j, t_args *arg, t_params *param)
{
	if (arg_is_zero(arg) == 1 && param->prec == 0 && param->is_prec == true)
		return ;
	fill_intmax_to_a(arg->arg.i, buf, j, param);
}

static void	fill_chars(char *buf, int *j, t_args *arg, t_params *param)
{
	int		len;
	char	*null;

	null = ft_strdup("(null)");
	if (arg->type == STR)
	{
		if (param->is_prec == true)
			len = (param->prec < (int)ft_strlen(arg->arg.s)) ?
				param->prec : ft_strlen(arg->arg.s);
		else
			len = ft_strlen(arg->arg.s);
		if (arg->arg.s)
		{
			ft_memmove(buf + *j, arg->arg.s, len);
			(*j) += len;
		}
		else
		{
			ft_memmove(buf + *j, null, 6);
			(*j) += 6;
		}
	}
	else if (arg->type == CHAR)
		buf[(*j)++] = arg->arg.c;
	free(null);
}

int			fill_types(char *buf, int *j, t_args *arg, t_params *param)
{
	if (arg->type == CHAR || arg->type == STR)
		fill_chars(buf, j, arg, param);
	else if (arg->type == WINT_T)
	{
		if ((fill_wchar(buf, j, arg->arg.wi_t) == -1))
			return (-1);
	}
	else if (arg->type == WCHAR_T)
	{
		if ((fill_wstr(buf, j, arg->arg.wc_t, param) == -1))
			return (-1);
	}
	else if (arg_is_int(arg))
		fill_signed(buf, j, arg, param);
	else if (arg_is_uint(arg))
		fill_unsigned(buf, j, arg, param);
	return (1);
}
