/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_size.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:01:30 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/08 19:57:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_size.h"

static int	calc_options(t_params *param, t_args *args, int size)
{
	t_args	*tmp;

	tmp = args;
	while (tmp->next)
		tmp = tmp->next;
	size = size_flags_part1(param, tmp, size);
	size = size_flags_part2(param, tmp, size);
	if (param->hash == 1 && (param->type == 'x' || param->type == 'X')
			&& arg_is_zero(args) == 0 && size - 2 < param->prec)
	{
		size += param->prec - (size - 2);
		param->len -= (param->prec - (size - 2)) + 2;
	}
	if (arg_is_positive(tmp) == 0 && param->is_prec == true
			&& size <= param->prec)
		size++;
	return (size);
}

static int	calc_types(t_params *param, va_list ap, t_args **args)
{
	int		size;
	t_args	*arg;

	if (param->type == 'd' || param->type == 'i' || param->type == 'D'
			|| param->type == 'u' || param->type == 'U' || param->type == 'o'
			|| param->type == 'O' || param->type == 'x' || param->type == 'X'
			|| param->type == 'c' || param->type == 'C' || param->type == 's'
			|| param->type == 'S' || param->type == 'p' || param->type == '%'
			|| param->type == 'b')
		size = size_types(param, args, ap);
	else
	{
		arg = push_back_new_arg(args);
		if (param->type == 0)
			return (0);
		arg->len = 1;
		arg->type = CHAR;
		arg->arg.c = param->type;
		size = 1;
	}
	size = calc_options(param, *args, size);
	return (size);
}

static int	to_end_of_flags(const char *format, int i, t_params *param)
{
	while (format[i] && format[i] != param->type)
		i++;
	return (i + 1);
}

int			calc_size(const char *format, t_params *params,
		va_list ap, t_args **args)
{
	int			size;
	int			i;
	int			len;
	t_params	*tmp;

	tmp = params;
	size = 0;
	i = 0;
	len = ft_strlen(format);
	while (i < len)
	{
		if (format[i] == '%')
		{
			size += calc_types(tmp, ap, args);
			i = to_end_of_flags(format, i + 1, tmp);
			tmp = tmp->next;
		}
		else
		{
			i++;
			size++;
		}
	}
	return (size);
}
