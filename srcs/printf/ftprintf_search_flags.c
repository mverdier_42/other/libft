/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_search_flags.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:01:03 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/08 19:52:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_search.h"

static void	search_type(const char *format, int *i, t_params *param)
{
	if (format[*i] == '#' || format[*i] == '+' || format[*i] == '-'
			|| format[*i] == ' ' || (format[*i] >= '0' && format[*i] <= '9')
			|| format[*i] == 'h' || format[*i] == 'l' || format[*i] == 'j'
			|| format[*i] == 'z' || format[*i] == '.')
		search_option(format, i, param);
	else
	{
		param->type = format[*i];
		*i += 1;
		if (param->type == 'p')
			param->hash = 1;
	}
}

static void	search_size(const char *format, int *i, t_params *param)
{
	if (format[*i] == 'h' && format[*i + 1] == 'h'
			&& size_is_higher(param, format, *i))
		param->size = 'H';
	else if (format[*i] == 'h' && size_is_higher(param, format, *i))
		param->size = 'h';
	else if (format[*i] == 'l' && format[*i + 1] == 'l'
			&& size_is_higher(param, format, *i))
		param->size = 'L';
	else if (format[*i] == 'l' && size_is_higher(param, format, *i))
		param->size = 'l';
	else if (format[*i] == 'j' && size_is_higher(param, format, *i))
		param->size = 'j';
	else if (format[*i] == 'z' && size_is_higher(param, format, *i))
		param->size = 'z';
	if ((format[*i] == 'l' || format[*i] == 'h')
			&& format[*i + 1] == format[*i])
		*i += 2;
	else if (format[*i] == 'h' || format[*i] == 'l' || format[*i] == 'j'
			|| format[*i] == 'z')
		*i += 1;
	search_type(format, i, param);
}

static void	search_precision(const char *format, int *i, t_params *param)
{
	if (format[*i] == '.')
	{
		*i += 1;
		if (format[*i] < '0' || format[*i] > '9')
			param->prec = 0;
		while (format[*i] >= '0' && format[*i] <= '9')
		{
			param->prec = (param->prec * 10) + (format[*i] - 48);
			*i += 1;
		}
		param->is_prec = true;
	}
	search_size(format, i, param);
}

static void	search_min_width(const char *format, int *i, t_params *param)
{
	if (format[*i] == '0' && (format[*i + 1] <= '0' || format[*i + 1] > '9'))
	{
		(*i)++;
		param->zero = 1;
		search_precision(format, i, param);
		return ;
	}
	while (format[*i] >= '0' && format[*i] <= '9')
	{
		param->width = (param->width * 10) + (format[*i] - 48);
		*i += 1;
	}
	search_precision(format, i, param);
}

void		search_option(const char *format, int *i, t_params *param)
{
	if (!format[*i])
	{
		param->type = 0;
		return ;
	}
	if (format[*i] == '#')
		param->hash = 1;
	else if (format[*i] == '0' && param->minus == 0)
		param->zero = 1;
	else if (format[*i] == '-')
	{
		param->minus = 1;
		param->zero = 0;
	}
	else if (format[*i] == '+')
		param->plus = 1;
	else if (format[*i] == ' ')
		param->space = 1;
	else
	{
		search_min_width(format, i, param);
		return ;
	}
	*i += 1;
	search_option(format, i, param);
}
