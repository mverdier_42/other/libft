/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_save_arg.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 16:00:49 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:00:57 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_save_arg.h"

char		*save_char_arg(t_args *arg, va_list ap)
{
	if (arg->type == WCHAR_T)
		return ((char *)(arg->arg.wc_t = va_arg(ap, wchar_t*)));
	else if (arg->type == STR)
	{
		if ((arg->arg.s = va_arg(ap, char*)) == NULL)
			arg->arg.s = "(null)";
		return (arg->arg.s);
	}
	return (NULL);
}

uintmax_t	save_uintmax_arg(t_args *arg, va_list ap, t_params *param)
{
	if (arg->stype == USINT)
		return ((arg->arg.ui = (unsigned short)va_arg(ap, unsigned int)));
	if (arg->stype == UCHAR)
		return ((arg->arg.ui = (unsigned char)va_arg(ap, unsigned int)));
	if (arg->type == UINT && param->type == 'p')
		return (arg->arg.ui = (uintmax_t)va_arg(ap, void *));
	if (arg->type == UINT)
		return ((arg->arg.ui = va_arg(ap, unsigned int)));
	if (arg->type == ULINT)
		return ((arg->arg.ui = va_arg(ap, unsigned long int)));
	if (arg->type == ULLINT)
		return (arg->arg.ui = va_arg(ap, unsigned long long int));
	if (arg->type == UINTMAX_T)
		return (arg->arg.ui = va_arg(ap, uintmax_t));
	if (arg->type == SIZE_T)
		return ((arg->arg.ui = va_arg(ap, size_t)));
	return (0);
}

intmax_t	save_intmax_arg(t_args *arg, va_list ap)
{
	if (arg->stype == SINT)
		return ((arg->arg.i = (short)va_arg(ap, int)));
	if (arg->stype == SCHAR)
		return ((arg->arg.i = (char)va_arg(ap, int)));
	if (arg->type == INT)
		return ((arg->arg.i = va_arg(ap, int)));
	if (arg->type == INTMAX_T)
		return ((arg->arg.i = va_arg(ap, intmax_t)));
	if (arg->type == LINT)
		return ((arg->arg.i = va_arg(ap, long int)));
	if (arg->type == LLINT)
		return ((arg->arg.i = va_arg(ap, long long int)));
	if (arg->type == SSIZE_T)
		return ((arg->arg.i = va_arg(ap, ssize_t)));
	return (0);
}
