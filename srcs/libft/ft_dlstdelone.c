/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstdelone.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 15:46:24 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/16 17:48:10 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	ft_dlstdelone(t_dlist **dlist, void (*del)(void*), t_dlist_b **dlist_b)
{
	t_dlist	*tmp;

	(*dlist)->prev->next = (*dlist)->next;
	(*dlist)->next->prev = (*dlist)->prev;
	tmp = (*dlist)->next;
	if ((*dlist) == (*dlist_b)->first && (*dlist) == (*dlist_b)->last)
	{
		(*dlist_b)->first = NULL;
		(*dlist_b)->last = NULL;
	}
	else if ((*dlist) == (*dlist_b)->first)
	{
		(*dlist_b)->first = (*dlist)->next;
		(*dlist_b)->last->next = (*dlist)->next;
	}
	else if ((*dlist) == (*dlist_b)->last)
	{
		(*dlist_b)->last = (*dlist)->prev;
		(*dlist_b)->first->prev = (*dlist)->prev;
	}
	(*del)((*dlist)->content);
	free(*dlist);
	(*dlist) = (*dlist_b)->first;
	(*dlist_b)->size--;
}
