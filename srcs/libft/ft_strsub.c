/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:52:03 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 16:33:55 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*newstr;
	size_t	i;

	if (!s)
		return (NULL);
	if ((newstr = ft_strnew(len)) == NULL)
		return (NULL);
	i = 0;
	while (i < len)
	{
		newstr[i] = s[start];
		i++;
		start++;
	}
	return (newstr);
}
