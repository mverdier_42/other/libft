# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2017/11/02 16:42:35 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#

NAME = 		libft.a

SRCDIR =	./srcs

OBJDIR =	./objs

INCDIR =	./includes

# List all sources, objects and includes files of 'libft'.

LIB_SRC =	ft_memset.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c	\
			ft_memchr.c ft_memcmp.c ft_strlen.c ft_strdup.c ft_strcpy.c		\
			ft_strncpy.c ft_strcat.c ft_strncat.c ft_strlcat.c ft_strchr.c	\
			ft_strrchr.c ft_strstr.c ft_strnstr.c ft_strcmp.c ft_strncmp.c	\
			ft_atoi.c ft_isalpha.c ft_isdigit.c ft_isalnum.c ft_isascii.c	\
			ft_isprint.c ft_toupper.c ft_tolower.c ft_memalloc.c ft_memdel.c\
			ft_strnew.c ft_strdel.c ft_strclr.c ft_striter.c ft_striteri.c	\
			ft_strmap.c ft_strmapi.c ft_strequ.c ft_strnequ.c ft_strsub.c	\
			ft_strjoin.c ft_strtrim.c ft_strsplit.c ft_itoa.c ft_putchar.c	\
			ft_putstr.c ft_putendl.c ft_putnbr.c ft_putchar_fd.c			\
			ft_putstr_fd.c ft_putendl_fd.c ft_putnbr_fd.c ft_lstnew.c		\
			ft_lstdelone.c ft_lstdel.c ft_lst_push_front.c ft_lstiter.c		\
			ft_lst_push_back.c ft_lstmap.c ft_strrev.c ft_strswap.c			\
			ft_sortstrtab.c ft_intswap.c ft_sortinttab.c ft_islower.c		\
			ft_isupper.c ft_isnumber.c ft_isblank.c ft_isspace.c			\
			ft_strtrimc.c ft_strndup.c ft_nrealloc.c ft_strdup_to_c.c		\
			ft_strsplit_str.c ft_str_test_chars.c ft_strextract.c			\
			ft_strrextract.c ft_dlstnew.c ft_dlst_push_back.c				\
			ft_dlst_push_front.c ft_dlstdel.c ft_dlstdelone.c				\
			ft_dlst_b_new.c ft_str_isdigit.c ft_free_tab_str.c

LIBDIR =	$(SRCDIR)/libft

LIBSRC =	$(LIB_SRC:%=$(LIBDIR)/%)

LIBOBJDIR =	$(OBJDIR)/libft

LIBOBJ =	$(LIB_SRC:%.c=$(LIBOBJDIR)/%.o)

LIBINCDIR =	$(INCDIR)/libft

LIBINC =	$(LIBINCDIR)/ft_int.h $(LIBINCDIR)/ft_lst.h	\
			$(LIBINCDIR)/ft_memory.h $(LIBINCDIR)/ft_string.h	\
			$(LIBINCDIR)/ft_dlst.h $(INCDIR)/libft.h

#------------------------------------------------------------------------------#
# List all sources, objects and includes files of 'get_next_line'.

GNL_SRC =	get_next_line.c

GNLDIR =	$(SRCDIR)/gnl

GNLSRC =	$(GNL_SRC:%=$(GNLDIR)/%)

GNLOBJDIR =	$(OBJDIR)/gnl

GNLOBJ =	$(GNL_SRC:%.c=$(GNLOBJDIR)/%.o)

GNLINCDIR =	$(INCDIR)/gnl

GNLINC =	$(GNLINCDIR)/get_next_line.h $(INCDIR)/libft.h

#------------------------------------------------------------------------------#
# List all sources, objects and includes files of 'ft_printf'.

FTP_SRC =	ft_printf.c ftprintf_search_flags.c ftprintf_search_params.c	\
			ftprintf_params.c ftprintf_args.c ftprintf_calc_chars.c			\
			ftprintf_calc_digits.c ftprintf_size.c ftprintf_lens_types.c	\
			ftprintf_save_arg.c ftprintf_fill.c ftprintf_fill_itoa.c		\
			ftprintf_fill_types.c ftprintf_fill_flags.c						\
			ftprintf_fill_width.c ftprintf_args_types.c						\
			ftprintf_fill_wchars.c ftprintf_size_flags.c

FTPDIR =	$(SRCDIR)/printf

FTPSRC =	$(FTP_SRC:%=$(FTPDIR)/%)

FTPOBJDIR =	$(OBJDIR)/printf

FTPOBJ =	$(FTP_SRC:%.c=$(FTPOBJDIR)/%.o)

FTPINCDIR =	$(INCDIR)/printf

FTPINC =	$(FTPINCDIR)/ft_printf.h $(FTPINCDIR)/ftprintf_args.h	\
			$(FTPINCDIR)/ftprintf_colors.h $(FTPINCDIR)/ftprintf_fill.h	\
			$(FTPINCDIR)/ftprintf_lens.h $(FTPINCDIR)/ftprintf_params.h	\
			$(FTPINCDIR)/ftprintf_save_arg.h $(FTPINCDIR)/ftprintf_search.h	\
			$(FTPINCDIR)/ftprintf_size.h $(INCDIR)/libft.h

#------------------------------------------------------------------------------#
# List general variables, containing libft, gnl and printf.

SRCS =		$(LIBSRC)		\
			$(GNLSRC)		\
			$(FTPSRC)

INCS =		$(LIBINC)		\
			$(GNLINC)		\
			$(FTPINC)

SRCDIRS =	$(SRCDIR)		\
			$(LIBDIR)		\
			$(GNLDIR)		\
			$(FTPDIR)

OBJS =		$(LIBOBJ)		\
			$(GNLOBJ)		\
			$(FTPOBJ)

OBJDIRS =	$(LIBOBJDIR)	\
			$(GNLOBJDIR)	\
			$(FTPOBJDIR)

INCDIRS =	$(INCDIR)		\
			$(LIBINCDIR)	\
			$(GNLINCDIR)	\
			$(FTPINCDIR)

#------------------------------------------------------------------------------#
# List all compilation variables.

CC =		gcc

CFLAGS =	-Wall			\
			-Wextra			\
			-Werror			\

INCFLAGS =	-I $(INCDIR)	\
			-I $(LIBINCDIR)	\
			-I $(GNLINCDIR)	\
			-I $(FTPINCDIR)

FLAGS =		$(CFLAGS)		\
			$(INCFLAGS)

#------------------------------------------------------------------------------#
# List all rules used to make libft.a

all:
	@$(MAKE) $(NAME)

$(NAME): $(OBJS)
	@$(MAKE) printname
	@printf "%-15s%s\n" Linking $@
	@ar -crs $@ $(OBJS)
	@printf "$(GREEN)"
	@echo "Compilation done !"
	@printf "$(RES)"

$(LIBOBJ): $(LIBINC)

$(GNLOBJ): $(GNLINC)

$(FTPOBJ): $(FTPINC)

$(LIBOBJDIR)/%.o: $(LIBDIR)/%.c
	@mkdir -p $(LIBOBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(GNLOBJDIR)/%.o: $(GNLDIR)/%.c
	@mkdir -p $(GNLOBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(FTPOBJDIR)/%.o: $(FTPDIR)/%.c
	@mkdir -p $(FTPOBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

printname:
	@printf "$(ORANGE)"
	@printf "[%-15s " "$(NAME)]"
	@printf "$(RES)"

clean:
	@$(MAKE) printname
	@echo Suppressing obj files
	@printf "$(RED)"
	rm -rf $(OBJS)
	@rm -rf $(OBJDIRS)
	@printf "$(RES)"

fclean: clean
	@$(MAKE) printname
	@echo Suppressing $(NAME)
	@printf "$(RED)"
	rm -rf $(NAME)
	@printf "$(RES)"

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

NORM = `norminette $(SRCS) $(INCS) | grep -B1 Error | cat`

no:
	@$(MAKE) printname
	@echo "Passage de la norminette :"
	@if [ "$(NORM)" == "`echo ""`" ];									\
		then															\
			echo "$(GREEN)Tous les fichiers sont a la norme !$(RES)";	\
		else															\
			echo "$(RED)$(NORM)$(RES)";									\
	fi

PRTF = `cat $(SRC) | grep printf | cat`

printf:
	@$(MAKE) printname
	@echo "Verifications des printf :"
	@if [ "$(PRTF)" == "`echo ""`" ];									\
		then															\
			echo "$(GREEN)Auncun fichier ne contient de printf !$(RES)";	\
		else															\
			echo "$(RED)$(PRTF)$(RES)";									\
	fi

check: no printf

# A rule to make git add easier

git:
	@$(MAKE) printname
	@echo Adding files to git repository
	@printf "$(GREEN)"
	git add $(SRCS) $(INCS) Makefile
	@printf "$(RES)"
	git status

.PHONY: all clean re fclean git no printf check
