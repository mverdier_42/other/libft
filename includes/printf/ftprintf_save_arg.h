/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_save_arg.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:47:55 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:48:23 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_SAVE_ARG_H
# define FTPRINTF_SAVE_ARG_H

# include "ftprintf_args.h"
# include "ftprintf_params.h"

# include <stdarg.h>
# include <inttypes.h>

intmax_t	save_intmax_arg(t_args *arg, va_list ap);
uintmax_t	save_uintmax_arg(t_args *arg, va_list ap, t_params *param);
char		*save_char_arg(t_args *arg, va_list ap);

#endif
