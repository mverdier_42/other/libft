/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:52:53 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/09 13:39:07 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdbool.h>

# include "libft/ft_memory.h"
# include "libft/ft_string.h"
# include "libft/ft_int.h"
# include "libft/ft_lst.h"
# include "libft/ft_dlst.h"

# include "gnl/get_next_line.h"

# include "printf/ft_printf.h"

#endif
